<?php
class BinaryFileSearcher
{
    private $file;
    private $adress;

    public function __construct($filename)
    {
        $this->file = fopen($filename, 'rb');
        if (!$this->file) {
            throw new Exception('Не удалось открыть файл');
        }
    }

    public function __destruct()
    {
        if ($this->file) {
            fclose($this->file);
        }
    }

    public function findSignature($signature)
    {
        $signature_length = strlen($signature);
        $size = filesize("bios.bin");

        $data = fread($this->file, $size);
            $pos = strpos($data, $signature);
            if ($pos !== false) {
                $this->adress=$pos+$signature_length;
                return dechex($this->adress);
            }

        return false;
    }

    public function get_key(){
        fseek($this->file,$this->adress);
        return fread($this->file,29);
    }
}

$options = getopt("d:s:h",['help','dump:']);
if (array_key_exists('h', $options) || array_key_exists('help', $options)) {
    echo "Использование: php sercher.php [-d|--dump <имя файла>] [-s|--signature <возраст>] [-h|--help]\n";
    echo "Описание:\n";
    echo "  -d, --dump         Указать путь к файлу ,поумолчанию bios.bin \n";
    echo "  -s, --signature    Указать сигнатуру файла, по умолчанию ищет ключ винды (29 символов после сигнатуры)\n";
    echo "  -h, --help         Показать эту справку\n";
    exit(1);
}


$file = $options['d'] ?? 'bios.bin';
$signature = $options['s'] ?? "\x01\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x1D\x00\x00\x00";
$searcher = new BinaryFileSearcher($file);
$address = $searcher->findSignature($signature);
if ($address) {
    var_dump($searcher->get_key());
} else {
    echo "Сигнатура не найдена";
}